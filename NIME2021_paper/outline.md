## Preliminary outline

Word limit? 5000? 6000? 

- Introduction - all
- Background
    - Intersection of NIME practice/music technology and environmental issues - Adam
    - NIME as a self reflective community - Raul
- Systematic review of NIME literature - Johnny/all
    - Keyword/collocation analysis of NIME (and possibly other conferences) proceedings (see https://gitlab.com/johnnyvenom/nime-keywords)
    - manual review of NIME abstracts
    - Parallel/similar communities: HCI; intermedia/interdisciplinary artistic practices
- ECO_NIME wiki - all
- Discussion/Conclusion