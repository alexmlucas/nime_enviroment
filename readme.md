# Environmental issues officer

Main goal: Work on how to reduce the environmental footprint of the NIME community

## Tasks:

- Support local chairs in how to run a more environmentally-friendly physical conference
- Develop strategies for alternative conference organization
- Develop guidelines for reducing the environmental footprint in NIME design. 

## Officers:

- Adam Pultz Melbye
- John Sullivan
- Raul Masu

## ECO_NIME

To provide information and resources on environmental topics for NIME researchers, we have created the **ECO_NIME wiki**, which is contained in a separate repo [here](https://gitlab.com/raulmasu/eco_nime).

## Alternative conference organization

- [The semi-virtual academic conference (a short documentary) - YouTube](https://www.youtube.com/watch?v=TPtDHidVyZE)
- [Virtual socializing at academic conferences](http://www.parncutt.org/virtualsocializing.html)
- [An analysis of ways to decarbonize conference travel after COVID-19](https://www.nature.com/articles/d41586-020-02057-2?s=09)


Examples of Virtual conferences:

- [CIM19](https://sites.google.com/view/cim19/home)
- [Neuronline](https://neuronline.sfn.org/Articles/Scientific-Research/2019/Machine-Learning-in-Neuroscience-Fundamentals-and-Possibilities)

## Time zone coordination

(for planning meetings)

Time zones (summer): change back Nov 1 (CA), Oct 25? (EU)

| UTC   | Johnny | Raul  | Adam  |
| -----:| -----:| -----:| -----:|
|       | **EST**   | **WEST**  | **CEST**  |
| **UTC+0** | **UTC-4** | **UTC+1** | **UTC+2** |
| 0h    | 20h   | 1h    | 2h    |
| 1h    | 21h   | 2h    | 3h    |
| 2h    | 22h   | 3h    | 4h    |
| 3h    | 23h   | 4h    | 5h    |
| 4h    | 0h    | 5h    | 6h    |
| 5h    | 1h    | 6h    | 7h    |
| 6h    | 2h    | 7h    | 8h    |
| 7h    | 3h    | 8h    | 9h    |
| 8h    | 4h    | 9h    | 10h   |
| 9h    | 5h    | 10h   | 11h   |
| 10h   | 6h    | 11h   | 12h   |
| 11h   | 7h    | 12h   | 13h   |
| 12h   | 8h    | 13h   | 14h   |
| 13h   | 9h    | 14h   | 15h   |
| 14h   | 10h   | 15h   | 16h   |
| 15h   | 11h   | 16h   | 17h   |
| 16h   | 12h   | 17h   | 18h   |
| 17h   | 13h   | 18h   | 19h   |
| 18h   | 14h   | 19h   | 20h   |
| 19h   | 15h   | 20h   | 21h   |
| 20h   | 16h   | 21h   | 22h   |
| 21h   | 17h   | 22h   | 23h   |
| 22h   | 18h   | 23h   | 0h    |
| 23h   | 19h   | 0h    | 1h    |