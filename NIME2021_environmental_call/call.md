# Final version

This year, we include a call for scientific and artistic contributions that specifically address environmental-related issues within and beyond NIME. Submissions are encouraged around the following topics:

- Environmental issues around NIME research 
- Artistic works or projects that highlight climate change and environmental issues around the world aiming to increase environmental awareness 
- Sustainable NIME design
- Recycling and retiring NIMEs and issues surrounding low-cost throw-away parts and prototypes
- Green technologies utilized in NIME design and evaluation (e.g., green hosting in web-based NIMEs, recyclable materials) 
- Critiques of techno-solutionism and critical perspectives on the sustainability of established and emergent technologies

For more information about NIME's commitment to the environment and to support authors in developing their submissions, we invite you to read our [statement on the environment](#). Additionally, we are launching a repository that will provide information and resources for a wide-ranging list of topics relevant to NIME and the environment.

----------------

# Notes:

## Raul 
list of possible topics:
- participapty projects that aim to increase environmental awareness in the process of making digital art  

- recicling in NIME
- green Hosting in web based / ML based NIMEs
- project that aim to increase environmental awareness in the process of making digital art  
- artisti projects that aim to increase environmental awareness (e.g., becouse of the topic or the technology used)


## Johnny's list of possible topics for call 

(I haven't looked at the list above, so maybe/hopefully plenty of overlap: )

...contributions are encouraged around the following topics:

- green technologies utilized in NIME design and evaluation
- environmental issues around NIME research
- sustainable NIME design
- artistic works that highlight climate change and environmental issues around the world
- recycling and retiring NIMEs

## Adam  

We welcome calls for papers that address the climate emergency and other planetary impacts of human activity.
Papers can describe NIMEs that give voice to environmental changes, are built from recyclable or compostable materials, harness wind energy or recycle their own energy expenditure.
- Critique of techno-solutionisms 
- Problems of low-cost throw-away parts and prototypes
- Links:
    Wind turbine: https://www.theguardian.com/environment/2020/sep/23/tiny-wind-turbine-can-collect-energy-from-a-walkers-swinging-arm
    Piezoelectricity from Dancing: http://large.stanford.edu/courses/2010/ph240/winger1/